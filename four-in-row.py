# Oskbol's Four in a Row with giant lasers, unicorns and cheese balls (I don't like cheese balls). Also there might be some other things, player direction is advised.

import matrix
import random
import time

existing_columns = ["1", "2", "3", "4", "5", "6", "7"]
special_marks = ["\033[1;35m#\033[1;m", "\033[1;34m*\033[1;m"]
fredde_status = 0

# Party Event Counter
cheeseball_count = 0
laser_count = 0
unicorn_count = 0
batman_count = 0
ufo_count = 0
snowman_count = 0
north_korea_count = 0
fredde_count = 0

def get_victory_msg(winner, loser):
    victory_msg = [winner + " just bitch slapped your ass with some defeat " + loser + ", maybe it's time to step your game up?",
    "It looks like " + winner + " gave you the taste of bitter defeat and total ownage " + loser + ", better luck next time!",
    "Could you withstand the force that is " + winner + ", " + loser + "? No, you couldn't because you suck.",
    "Seriously " + loser + "? Get rekt. Get Shrekt. Get Reject. Get some protect because this defeat by " + winner + " will haunt you forever.",
    "Toot your airhorns boys because " + winner + " just showed " + loser + " some disrespect!",
    "The power, the moves, the tactics and the annihilation is real. " + loser + " just got blown away by " + winner + "!"]
    return random.sample(victory_msg, 1)[0]

def get_players():
    player1 = player_name("Who is Player 1?")
    pl1_mark = player_mark("What mark do you want to use " + player1 + "?", player1)
    player2 = player_name("Who is Player 2?")
    pl2_mark = player_mark("What mark do you want to use " + player2 + "?", player2)
    while player1 == player2:
        print("Sorry Player 2 your name is equal to Player 1, please change it.")
        player2 = player_name("Who is Player 2?")
    while pl1_mark == pl2_mark:
        print("Sorry " + player2 + " your mark is equal to " + player1 + ", please change it.")
        pl2_mark = player_mark("What mark do you want to use " + player2 + "?", player2)
    print("Good luck " + player1 + " and " + player2 + "!")
    return player1, player2, pl1_mark, pl2_mark

def player_name(msg):
    print(msg)
    player = input("> ")
    if len(player) == 0 or player[0] == " ":
        print("Don't be shy.")
        return player_name(msg)
    print("Nice to meet you " + player + "!")
    return player
    
def player_mark(msg, player):
    print(msg)
    mark = input("> ")
    if len(mark) == 0 or mark[0] == " " or len(mark) > 1 or mark in special_marks:
        print("Sorry that mark is invalid " + player + "!")
        return player_mark(msg, player)
    print("Good choice " + player + ", your mark is now " + mark + "!")
    return mark
    
def party_mode():
    print("\033[1;31mWARNING!")
    print("This is not for the casual players, proceed with caution.\033[1;m")
    print("Do you want to play in Party Mode?")
    print("Inputting \033[1;35mparty\033[1;m will enable Party Mode.")
    print("If you don't want to play input anything else.")
    party = input("> ").lower()
    if party == "party":
        print("\033[1;35mParty Mode activated\033[1;m")
    elif party == "extreme party":
        print("\033[1;35mSecret Extreme Party Mode activated\033[1;m")
    else:
        print("Decided not to play in Party Mode.")
    return party

def play_game(player1, player2, pl1_mark, pl2_mark, party):
    rounds = 0
    victory = False
    full = False
    matris = matrix.Matrix(6,7)
    print(matris)
    while full == False:
        rounds += 1
        # Player 1
        matris = player_move(player1, matris, pl1_mark)
        winner, loser, victory = find_victory(matris, player1, player2, pl1_mark, pl2_mark)
        if victory == True:
            break
        if matrixFull(matris):
            break
        matris = party_main(matris, party, pl1_mark, pl2_mark)
        winner, loser, victory = find_victory(matris, player1, player2, pl1_mark, pl2_mark)
        if victory == True:
            break
        if matrixFull(matris):
            break
        # Player 2
        matris = player_move(player2, matris, pl2_mark)
        winner, loser, victory = find_victory(matris, player1, player2, pl1_mark, pl2_mark)
        if victory == True:
            break
        if matrixFull(matris):
            break
        matris = party_main(matris, party, pl1_mark, pl2_mark)
        winner, loser, victory = find_victory(matris, player1, player2, pl1_mark, pl2_mark)
        if victory == True:
            break
        if matrixFull(matris):
            break
        # End of round
    if victory == True:
        return victory, winner, loser, rounds
    else:
        print("Good game " + player1 + " and " + player2 + ", this time no one is a winner or a wiener.")
        return victory, winner, loser, rounds

def player_move(player, matris, mark):
    global existing_columns
    while True:
        print("In which column would " + player + " like to place their marker?")
        move = input("> ")
        if move not in existing_columns:
            print("That column doesn't exist " + player + "! I hope you're just pretending to be an idiot.")
        else:
            move = int(move)
            move -= 1
            mark_row = which_row(move, matris)
            if mark_row == -1:
                print("That column is full " + player + "! I hope you're just pretending to be an idiot.")
            else:
                matris[mark_row,move] = mark
                break
    print(matris)
    return matris
    
def find_victory(matris, player1, player2, pl1_mark, pl2_mark):
    victory = find_pattern(matris, pl1_mark * 4)
    if victory == True:
        winner = player1
        loser = player2
        return winner, loser, victory
    victory = find_pattern(matris, pl2_mark * 4)
    if victory == True:
        winner = player2
        loser = player1
        return winner, loser, victory
    else:
        return False, False, False

def party_main(matris, party, pl1_mark, pl2_mark):
    global fredde_status
    events = [no_event, cheeseball, laser, unicorn, batman, ufo, snowman, north_korea, fredde]
    event_weights = [0, 30, 34, 38, 43, 46, 50, 53, 55, 56]
    # Which Party?
    if party == "party":
        which_party = random.randint(0,55)
    elif party == "extreme party":
        which_party = random.randint(30,55)
    else:
        which_party = 0
    #Fredde
    if fredde_status == 1:
        which_party = 55 
    elif fredde_status == 2:
        which_party = random.randint(30,54)
    #Activating Party Event
    for i in range(len(events)):
        event_weights[i]
        if which_party in range(event_weights[i], event_weights[i + 1]):
            events[i](matris, party, pl1_mark, pl2_mark)
    return matris
    
def no_event(matris, party, pl1_mark, pl2_mark):
    return matris

def cheeseball(matris, party, pl1_mark, pl2_mark):
    global cheeseball_count
    print("\n\033[1;33mA massive Cheeseball Avalanche is approaching!\033[1;m\n")
    column = random.randint(0,6)
    for row in range(6):
        matris[row,column] = " "
    time.sleep(1)
    print(matris)
    column = column + 1
    print("\n\033[1;33mThe avalanche removed all marks from column " + str(column) + "!\033[1;m\n")
    cheeseball_count += 1
    return matris
    
def laser(matris, party, pl1_mark, pl2_mark):
    global laser_count
    print("\n\033[1;31mA giant laser is approaching!\033[1;m\n")
    row = random.randint(0,5)
    for column in range(7):
        matris[row,column] = " "
    time.sleep(1)
    print(matris)
    time.sleep(1)
    for x in range(7):
        y = row
        while y < 5 and not (matris[y + 1,x] in special_marks):
            matris[y,x] = matris[y + 1,x]
            y += 1
    for column in range(7):
        if matris[5,column] not in special_marks:
            matris[5,column] = " "
    print(matris)
    row = row + 1
    print("\n\033[1;31mThe giant laser fired a beam that removed all marks from row " + str(row) + "!\033[1;m\n")
    laser_count += 1
    return matris
    
def unicorn(matris, party, pl1_mark, pl2_mark):
    global unicorn_count
    print("\n\033[1;35mA horde of unicorns is approaching!\033[1;m\n")
    column = random.randint(0,6)
    row = random.randint(0,5)
    matris[row,column] = "\033[1;35m#\033[1;m"
    time.sleep(1)
    print(matris)
    print("\n\033[1;35mOne of the unicorns pooped in the matrix!\033[1;m\n")
    unicorn_count += 1
    return matris
    
def batman(matris, party, pl1_mark, pl2_mark):
    global batman_count
    print("\n\033[1;30mBatman appeared!\033[1;m\n")
    for column in range(matris.getNumColumns()):
        for row in range(matris.getNumRows()):
            if matris[row,column] == pl1_mark:
                matris[row,column] = pl2_mark
            elif matris[row,column] == pl2_mark:
                matris[row,column] = pl1_mark
    time.sleep(1)
    print(matris)
    print("\n\033[1;30mBecause Batman is Batman, Batman switched the places of " + pl1_mark + " and " + pl2_mark + "!\033[1;m\n")
    batman_count += 1
    return matris
    
def ufo(matris, party, pl1_mark, pl2_mark):
    global ufo_count
    print("\n\033[1;37mA U.F.O made of pancakes is approaching!\033[1;m\n")
    which_mark = random.randint(1,2)
    if which_mark == 1:
        pl_mark = pl1_mark
    else:
        pl_mark = pl2_mark
    column = random.randint(0,6)
    row = which_row(column, matris)
    if row == -1:
        time.sleep(1)
        print("\n\033[1;37mThe Pancake U.F.O failed to spit out one " + pl_mark + "!\033[1;m\n")
        return matris
    matris[row,column] = pl_mark
    time.sleep(1)
    print(matris)
    print("\n\033[1;37mThe Pancake U.F.O spit out one " + pl_mark + "!\033[1;m\n")
    ufo_count += 1
    return matris
    
def snowman(matris, party, pl1_mark, pl2_mark):
    global snowman_count
    print("\n\033[1;36mDo you wanna build a snowman?\033[1;m\n")
    column = random.randint(0,6)
    for row in range(2):
        matris[row,column] = "\033[1;34m*\033[1;m"
    time.sleep(1)
    print(matris)
    column = column + 1
    print("\n\033[1;36mA snowman was built in column " + str(column) + "!\033[1;m\n")
    snowman_count += 1
    return matris
    
def north_korea(matris, party, pl1_mark, pl2_mark):
    global north_korea_count
    print("\n\033[1;34mA North Korean Fleet is approaching!\n\033[1;m")
    for column in range(7):
        matris[top_marker(matris, column), column] = " "
    time.sleep(1)
    print(matris)
    print("\n\033[1;34mNorth Korea dropped some bombs in the matrix!\n\033[1;m")
    north_korea_count += 1
    return matris
    
def fredde(matris, party, pl1_mark, pl2_mark):
    global fredde_status, fredde_count
    if fredde_status == 0:
        print("\n\033[1;32mFredde is approaching!\033[1;m\n")
        fredde_status = 1
        return matris
    elif fredde_status == 1:
        fredde_status = 2
        print("\n\033[1;32mFredde has reached the Party Mode Control Room!\033[1;m\n")
        time.sleep(1.5)
        print("\n\033[1;32mFredde: What happens if you press this button?\033[1;m\n")
        time.sleep(1.5)
        for i in range(3):
            matris = party_main(matris, party, pl1_mark, pl2_mark)
        time.sleep(1)
        print("\n\033[1;32mFredde left the game...\033[1;m\n")
        fredde_status = 0
        fredde_count += 1
        return matris
            
def top_marker(matris, column):
    column_list = get_column_list(matris, column)
    for i in range(5, -1, -1):
        if column_list[i] != " ":
            return i
    return i

def get_row(matris, rad):
    row = ""
    for index in range(matris.getNumColumns()):
        row += (matris[rad, index])
    return row

def get_column(matris, kolumn):
    return "".join(get_column_list(matris, kolumn))
    
def get_column_list(matris, kolumn):
    column = []
    for index in range(matris.getNumRows()):
        column.append(matris[index, kolumn])
    return column

def get_diagonal(matris, diagonal):
    diagonal_list = []
    for column in range(-2, 4):
        diagonal_str = ""
        # Diagonals from left to right
        for row in range(6):
            if square_exists(matris, row, column) == True:
                diagonal_str += (matris[row, column])
            column += 1
        diagonal_list.append(diagonal_str)
        # Diagonals from right to left
        column = column - 6
        for row in range(6):
            row = 5 - row
            if square_exists(matris, row, column) == True:
                diagonal_str += (matris[row, column])
            column += 1
        diagonal_list.append(diagonal_str)
    return diagonal_list[diagonal]
    
def square_exists(matris, row, column):
    if row < 0 or row >= matris.getNumRows() or column < 0 or column >= matris.getNumColumns():
        return False
    else:
        return True
    
def which_row(move, matris):
    column = get_column_list(matris, move)
    if ' ' in column:
        index = column.index(' ')
    else:
        index = -1
    return index
    
def matrixFull(matris):
    row = get_row(matris, 5)
    if not " " in row:
        return True
    else:
        return False
        
def find_pattern(matris, pattern):
    for row in range(matris.getNumRows()):
        if pattern in get_row(matris, row):
            return True
    for column in range(matris.getNumColumns()):
        if pattern in get_column(matris, column):
            return True
    for diagonal in range(12):
        if pattern in get_diagonal(matris, diagonal):
            return True

def results(winner, loser, victory, rounds, party):
    if victory == True:
        victory_msg = get_victory_msg(winner, loser)
        print(victory_msg)
        if rounds <= 4:
            print("It only took you " + str(rounds) + " rounds to lose " + loser + "!")
            print("I don't mean to hurt your feelings but you kinda suck, sorry...")
        elif rounds <= 7:
            print("You were crushed in a mere " + str(rounds) + " rounds " + loser + "!")
            print("It could be worse, right...?")
        elif rounds <=12:
            print(winner + " executed you in " + str(rounds) + " quick rounds " + loser + "!")
            print("Yeah, there's absolutely some room for improvment here...")
        elif rounds <= 16:
            print("You lasted an impressive " + str(rounds) + " rounds against " + winner + ", " + loser + "!")
            print("At least you did fight back " + loser + ", altough not enough.")
        elif rounds <= 21:
            print("Good game both of you, this amazing game lasted a whopping " + str(rounds) + " rounds!")
            print("I think " + loser + " is pretty thirsty for some revenge on " + winner + " though.")
        else:
            print(str(rounds) + " rounds in a regular game of Connect Four is impossible.")
            print("Party Mode right?")
        if party == "party" or party == "extreme party":
            party_results()
            
def party_results():
    if cheeseball_count == 1:
        print("\033[1;33m1 Cheeseball Avalanche terrorized the matrix!\033[1;m")
    else:
        print("\033[1;33m" + str(cheeseball_count) + " Cheeseball Avalanches terrorized the matrix!\033[1;m")
    if laser_count == 1:
        print("\033[1;31m1 Giant Laser Beam was fired!\033[1;m")
    else:
        print("\033[1;31m" + str(laser_count) + " Giant Laser Beams were fired!\033[1;m")
    if unicorn_count == 1:
        print("\033[1;35m1 Unicorn used the matrix as a toilet!\033[1;m")
    else:
        print("\033[1;35m" + str(unicorn_count) + " Unicorns used the matrix as a toilet!\033[1;m")
    if batman_count == 1:
        print("\033[1;30mBatman left the Batcave 1 time!\033[1;m")
    else:
        print("\033[1;30mBatman left the Batcave " + str(batman_count) + " times!\033[1;m")
    if ufo_count == 1:
        print("\033[1;37mThe Pancake Aliens visited 1 time!\033[1;m")
    else:
        print("\033[1;37mThe Pancake Aliens visited " + str(ufo_count) + " times!\033[1;m")
    if snowman_count == 1:
        print("\033[1;36m1 snowman was built!\033[1;m")
    else:
        print("\033[1;36m" + str(snowman_count) + " snowmen were built!\033[1;m")
    if north_korea_count == 1:
        print("\033[1;34mKim Jong-Un was bored 1 time!\033[1;m")
    else:
        print("\033[1;34mKim Jong-Un was bored " + str(north_korea_count) + " times!\033[1;m")
    if fredde_count == 1:
        print("\033[1;32mThe Beer Belly himself appeared once!\033[1;m")
    else:
        print("\033[1;32mThe Beer Belly himself appeared " + str(fredde_count) + " times!\033[1;m")
            
def main():
    print("Welcome to Connect Four made by Oskise.")
    print("Version: 1.1.2")
    time.sleep(1)
    player1, player2, pl1_mark, pl2_mark = get_players()
    party = party_mode()
    victory, winner, loser, rounds = play_game(player1, player2, pl1_mark, pl2_mark, party)
    results(winner, loser, victory, rounds, party)

main()

# Version History
# 1.0 - Default Connect Four
# 1.1 - Added Party Mode
# 1.1.1 - Changed chance of Party Events occuring, added Party Results
# 1.1.2 - Fixed a bug with the laser
