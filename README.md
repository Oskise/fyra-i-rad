# Fyra i rad #

Du kan använda dig av klassen som finns i matrix.py. En klass är ett object med givna metoder och atributer förknippade. Den klassen som finns i matrix.py heter Matrix. Matrix är en spelplan som du kan använda dig av i ditt fyra i rad spel. För att skapa en ny spelplan skriver du:

```
#!python
Matrix(antal rader, antal kolumer)

```
Om du sedan väljer att använda print på ett Matrix-objekt så kommer det automatiskt att skapa ett rutnät på följande sätt och siffror åvanför som i exemplet nedan:
```
#!python
matris = Matrix(10, 10)
print(matris)
 1 2 3 4 5 6 7 8 9 10
_____________________
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
---------------------
```
Du kan även få ut ett index på en given ruta igenoma att skriva:
```
#!python
m = Matrix(6,7)
m[4,2]
' '
```
Notera att det är den nedre vänstra rutan som är 0, 0!

Matrix har även två metoder för att ta reda på hur många rader respektive kolumer en matris har. Dessa metoder heter getNumRows och getNumColumns. För att använda dessa skriver du:
```
#!python
matris = Matrix(6,7)

matris.getNumRows()
6
matris.getNumColumns()
7
```


Det du ska göra är att forka gitten så att du kan göra ditt egna fyra i rad spel. Du behöver skapa ett nytt dokument och sedan importera matrix.py. Därefter kan du använda dig av Matrix för att göra ditt fyra i rad spel.
